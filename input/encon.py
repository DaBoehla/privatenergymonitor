import tools
import settings
from daynnight import isDay
import rrdtool
import time

class EnergyConverter:
    totalkwhday = 0.0
    totalkwhnight = 0.0
    lastimp = 0.0
    curCons = 0.0
    daycounter = 0
    nightcounter = 0
    lastupd = 0
    
    def getTotal(self):
        return self.totalkwhday + self.totalkwhnight
        
    def init(self):
        info = rrdtool.info(settings.rrdfile)
        print "last updated:" + str(info['last_update'])
        self.lastupd = long(info['last_update'])
        self.loadstate()
        print("Initialised value: day=" + str(self.totalkwhday) + "kwh night=" + str(self.totalkwhnight) + "kwh")
        
    def addImps(self, que):
        c = -1
        for item in que:
            c += 1
            thistime = long(que[c])
            if isDay(thistime):
                self.daycounter += 1
                self.totalkwhday += 1.0 / settings.impkwh
            else:
                self.nightcounter += 1
                self.totalkwhnight += 1.0 / settings.impkwh
            if thistime - self.lastupd <= 0:
                continue
            self.lastupd = thistime
            addstr = str(thistime) + ":" + str(self.daycounter) + ":" + str(self.nightcounter)
            rrdtool.update(settings.rrdfile, addstr)
            print("  addrrd impulse:" + addstr)
        
        ene = float(len(que)) / settings.impkwh
        if(self.lastimp > 20):
            timespan = float(que[-1]) - self.lastimp
        
            self.curCons = 3600 * 1000 * ene / timespan 
            
            print("totalday=" + str(self.totalkwhday) + "kwh; totalnight=" + str(self.totalkwhnight) + "kwh; power=" + str(self.curCons) + "W")
        self.lastimp = float(que[-1])
        self.savestate()
        
    def getNextImpulse(self):
        if self.curCons < 1:
            return 0
        pulsedur = 3600.0 * 1000/ self.curCons / settings.impkwh
        timeuntil = self.lastupd + pulsedur - time.time()
        return timeuntil
                
    def savestate(self):
        savevals = [ self.totalkwhday, self.totalkwhnight, self.daycounter, self.nightcounter ]
        outval = ""
        
        for item in savevals:
            if len(outval) > 0:
                outval += "\n"
            outval += str(item)
        
        tools.filewrite(settings.statefile, outval)
    def loadstate(self):
        read = tools.fileread(settings.statefile)
        lines = read.split('\n');
        if len(lines) < 4:
            return
        
        self.totalkwhday = float(lines[0])
        self.totalkwhnight = float(lines[1])
        self.daycounter = int(lines[2])
        self.nightcounter = int(lines[3])
        print(str(lines))
    
    