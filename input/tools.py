import sys

def filewrite(file, value):
    try:
        f = open(file,'w')
        f.write(str(value)) # python will convert \n to os.linesep
        f.close() # you can omit in most cases as the destructor will call if
    except:
        print("error :" + str(sys.exc_info()))
    
def fileread(file):
    try:
        f = open(file,'r')
        ret = f.read()
        f.close() # you can omit in most cases as the destructor will call if
        return ret
    except:
        print("error :" + str(sys.exc_info()))
        return ""
        
def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False
        
def RepresentsFloat(s):
    try: 
        float(s)
        return True
    except ValueError:
        return False