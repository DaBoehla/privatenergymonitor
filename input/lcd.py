#!/usr/bin/env python2
# Example using a character LCD plate.
import math
import time
import share
from thread import start_new_thread
import sys

import Adafruit_CharLCD as LCD

class LCDManager:
    colorstate = 2
    msgstate = 0
    buttons = ( LCD.SELECT,
                LCD.LEFT,
                LCD.UP,
                LCD.DOWN,
                LCD.RIGHT )
    
    colors = (  (0, 0, 0),
                (1, 0, 0),
                (0, 1, 0),
                (0, 0, 1),
                (1, 1, 0),
                (0, 1, 1),
                (1, 1, 0),
                (1, 1, 1))
                
    def init(self):
        start_new_thread(self.startLoop,())
        print "Lcd Display running :) ..."
        
    lcdmsgbuffer = ""
    def refresh(self):        
        tmpmsg = self.getMessage(self.msgstate)
        if not tmpmsg == self.lcdmsgbuffer:
            #self.lcd.clear()
            self.lcd.home()
            self.lcdmsgbuffer = tmpmsg
            self.lcd.message(tmpmsg)
        
        self.lcd.set_color(self.colors[self.colorstate][0], self.colors[self.colorstate][1], self.colors[self.colorstate][2])
    
    
    def getMessage(self, state):
        maxstates = 3
        state = state % maxstates
        share.locklcd.acquire()
        ret = ""
        lines = ["", ""]
        if state == 0:
            lines[0] = "%.2f" % share.endata.getTotal() + "kWh"
            lines[1] = "%#5d" % share.endata.curCons + "W"
        if state == 1:
            lines[0] = "Day:   " + "%.2f" % share.endata.totalkwhday + "kWh"
            lines[1] = "Night: " + "%.2f" % share.endata.totalkwhnight + "kWh"
        if state == 2:
            lines[0] = "Imp: " + "%#11d" % (share.endata.daycounter + share.endata.nightcounter)
            lines[1] = "%#15d" % int(share.endata.getNextImpulse()) + "s"
        i = 0
        for line in lines:
            while len(lines[i]) < 16:
                lines[i] += " "
            i += 1
            
        share.locklcd.release()
        return lines[0] + "\n" + lines[1]
            
    def startLoop(self):
        while True:
            try:
                self.lcd = LCD.Adafruit_CharLCDPlate()
                self.startMainLoop();
            except:
                print "LCD error:", sys.exc_info()
                time.sleep(1)
        
        
    def startMainLoop(self):
        print "Starting lcd display.."
        self.lcd.clear()
        self.lcd.create_char(1, [3,4,30,8,30,8,7,0])
        self.lcdmsgbuffer = ""
        self.refresh()
        ispressing = False
        loopdur = 0.1
        refreshtime = 2
        cref = 0
        while True:
            allreleased = True
            for button in self.buttons:
                if self.lcd.is_pressed(button): 
                    allreleased = False
                if self.lcd.is_pressed(button) and not ispressing:
                    # Button is pressed, change the message and backlight.
                    if button == LCD.UP:
                        self.colorstate += 1
                    if button == LCD.DOWN:
                        self.colorstate -= 1
                    self.colorstate = self.colorstate % len(self.colors)
                    if button == LCD.RIGHT:
                        self.msgstate += 1
                    if button == LCD.LEFT:
                        self.msgstate -= 1 		
                    self.refresh()
                    ispressing = True
                    cref = 0
                    self.lcd.set_color(self.colors[self.colorstate][0], self.colors[self.colorstate][1], self.colors[self.colorstate][2])
            if allreleased and ispressing:
                ispressing = False
            cref += 1
            if cref * loopdur > refreshtime:
                self.refresh()
                cref = 0
            time.sleep(loopdur)
            
            
            