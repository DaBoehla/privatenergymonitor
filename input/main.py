#!/usr/bin/env python2

from looper import startLooping
import time
from thread import start_new_thread
import share

import settings
import encon
from lcd import LCDManager


print "Starting main thread.."

myque = []
co = 0

share.init()

lc = LCDManager()
lc.init()

print("Counter initialised with :" + str(share.counter))

print "starting gpio thread..."
start_new_thread(startLooping,())



while True:
    time.sleep(2)
    share.lock.acquire()
    thiscou = share.counter
    myque.extend(share.dates)
    share.dates = []
    share.lock.release()

    if(len(myque) > 0):
        share.locklcd.acquire()
        share.endata.addImps(myque)
        share.locklcd.release()
        

    
    for item in myque:
        co += 1
    myque = []	

