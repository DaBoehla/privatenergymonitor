from thread import start_new_thread, allocate_lock
from encon import EnergyConverter

def init():
    global counter
    global lock
    global locklcd
    global dates
    global endata
    counter = 0
    lock = allocate_lock()
    locklcd = allocate_lock()
    dates = [] 
    endata = EnergyConverter()
    endata.init()
    
