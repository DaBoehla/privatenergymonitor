#!/usr/bin/env python2
from multiprocessing.connection import Client
from array import array
import time
import share
import sys

def startLooping():

    while True:
        try:
            address = ('localhost', 6000)
            conn = Client(address, authkey='secret password')
            while True:
                try:
                    msg = conn.recv()
                    conn.send("ok")
                    print msg
                    share.lock.acquire()
                    share.dates.extend(msg)
                    share.lock.release()
                    time.sleep(1)
                except:
                    print "Unexpected error:", sys.exc_info()
                    time.sleep(10)
                    break
        except:
            print "Unexpected error:", sys.exc_info()
            time.sleep(10)
            conn.close()
    
