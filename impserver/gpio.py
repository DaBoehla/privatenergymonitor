#!/usr/bin/env python2
import time
from random import randint
import share
import datetime
import RPi.GPIO as GPIO
import rrdtool
import sys
import settings

def startLooping():
    
    print("Starting io read looper")
    c = 0
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(settings.inputpin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    print("io read looper started")
    while True:
        try:
            debug = False
            if debug:
                timsl = randint(1, 10)
                time.sleep(float(timsl) / 50)
            else:
                GPIO.wait_for_edge(settings.inputpin, GPIO.FALLING) 
            
            share.lock.acquire()
            share.counter += 1
            now = time.time()
            share.dates.append(now)
            buffsiz = len(share.dates)
            share.lock.release()
            
            c = c + 1
            print("Received Edge "  + str(now) + " c=" + str(c) + " buffsize=" + str(buffsiz))
        except:
            print("error :" + str(sys.exc_info()))
    return
