from thread import start_new_thread, allocate_lock

def init():
    global lock
    global dates
    global counter
    lock = allocate_lock()
    dates = [] 
    counter = 0
    
