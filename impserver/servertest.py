from multiprocessing.connection import Client
from array import array
import time

address = ('localhost', 6000)
conn = Client(address, authkey='secret password')
while True:
    msg = conn.recv()
    conn.send("ok")
    print("1: " + str(msg[0]))
    print msg
    time.sleep(1)
# can also send arbitrary objects:
# conn.send(['a', 2.5, None, int, sum])
conn.close()