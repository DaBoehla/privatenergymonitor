from multiprocessing.connection import Listener
from array import array
import time
import share
from gpio import startLooping
from thread import start_new_thread
import sys

address = ('localhost', 6000)     # family is deduced to be 'AF_INET'
listener = Listener(address, authkey='secret password')



share.init()

print "starting gpio thread..."
start_new_thread(startLooping,())

myque = []

while True:
    conn = listener.accept()
    print 'connection accepted from', listener.last_accepted
    
    try:
        while True:
            
            share.lock.acquire()
            myque.extend(share.dates)
            share.dates = []
            share.lock.release()
            if len(myque) > 0:
                print("trysend...")
                conn.send(myque)
                time.sleep(5)
                msg = conn.recv()
                print("received: " + str(msg))
                if msg == "ok":
                    myque = []
            time.sleep(5)
    except:
        print "Unexpected error:", sys.exc_info()
listener.close()

